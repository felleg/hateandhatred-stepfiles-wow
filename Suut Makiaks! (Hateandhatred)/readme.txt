Included with this stepfile is:
- A Marja Tyrni Noteskin
- A Marja Tyrni Toasty
Both made by Eppujoloz
(and both in tyrni.zip)

This file is unreleased, it was "commissioned" by
Eppujoloz and also serves as a sequel to
Pomppufiilis, which I stepped around a decade ago.
The graphics by Eppujoloz were
made to celebrate the completion of the file,
and as such, I feel like it has strong identity.

This file is also an upgraded of the version
Snover played on stream. I have made changes
after hearing his comments about it, and I believe
he will enjoy the update.

For the non-Finnish speakers, the main change in question
is when the song goes "Eteen ja Taakse"
It means "forwards and backwards" and
the stepping reflects this.